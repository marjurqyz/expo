import React from "react";
import { View, Text, StyleSheet } from 'react-native';

const LayoutScreen = () => {
    return (
        <View style={style.viewStyle}>
            <Text style={style.textChild1Style}>Child 1</Text>
            <Text style={style.textChild2Style}>Child 2</Text>
            <Text style={style.textChild3Style}>Child 3</Text>
        </View>
    )
}

const style = StyleSheet.create(
    {
        viewStyle: {
            borderColor: 'black',
            borderWidth: 2,
            height: 400,

            //alignItems: 'stretch',
            //alignItems: 'flex-start'
            //alignItems: 'flex-end'
            //alignItems: 'center'
            //alignItems: 'baseline',

          //  flexDirection: 'column',
            //flexDirection: 'row',

           // justifyContent: 'center',
            //justifyContent: 'flex-start',
           // justifyContent: 'flex-end',
           // justifyContent: 'space-between',
         //   justifyContent: 'space-around',
           //justifyContent: 'space-evenly'


        },

        textChild1Style: {
            borderColor: 'blue',
            borderWidth: 1,
            //alignSelf: 'center'
          //  flex: 1
        },

        textChild2Style: {
            borderColor: 'blue',
            borderWidth: 1,
            position: 'absolute',
            //top: 100,
            //bottom: 100
          //  left: 100,
            right: 100
        
          // flex: 2
        },
        textChild3Style: {
            borderColor: 'blue',
            borderWidth: 1,
          //  flex: 6
        }

        // textStyle: {
        //     borderColor: 'blue',
        //     borderWidth: 1
       
        //     // marginTop: 20,
        //     // marginBottom: 20,
        //     // marginLeft: 20,
        //     // marginRight:20
        //     //marginHorizontal: 20,
        //     //marginVertical: 20,
        //     //margin: 20,

        //     //borderWidth: 2,
        //     //borderTopWidth: 2,
        //     //borderBottomWidth: 2,
        //     //borderLeftWidth: 2,
        //     //borderRightWidth: 2

        //     // paddingTop: 10,
        //     // paddingBottom: 10,
        //     // paddingLeft: 10,
        //     // paddingRight: 10,
        //     //paddingHorizontal: 10,
        //     //paddingVertical: 10,
        //     //padding: 10
        // }

    }
)

export default LayoutScreen;