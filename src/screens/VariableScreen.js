import React from "react";
import { View, Text } from 'react-native';

const VariableScreen = () => {
    let name = "Imię";
    let element = <Text>test123</Text>;
    const tab = ['tekst1', 'teskst2'];
    const obj = { name: "Michał", surname: "Orzechowski" };
    const tabObj = [
        { name: "Michał", surname: "Orzechowski" },
        { name: "Kamil", surname: "Kowalczyk" }
    ];
    const [a, b] = ['test1', 'test2'];
    //let a = 'test1';
    //let b = 'test2';

    return (
        <View>
            <View>
                <Text>Nazywam się {name}</Text>
                {element}
            </View>
            <View>
                <Text>Tab1 = {tab[0]}</Text>
                <Text>Tab2 = {tab[1]}</Text>
            </View>
            <View>
                <Text>Nazywam się {obj.name} {obj.surname}</Text>
            </View>
            <View>
                <Text>Nazywam się {tabObj[0].name} {tabObj[0].surname}</Text>
                <Text>Nazywam się {tabObj[1].name} {tabObj[1].surname}</Text>
                <Text>Zmienna a = {a}</Text>
                <Text>Zmienna b = {b}</Text>
            </View>
        </View>
    )
}

export default VariableScreen;