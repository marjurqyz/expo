import React, { useState } from "react";
import { View, Text, FlatList, StyleSheet } from 'react-native';

const ListScreen = () => {

    const friends = [
        { name: "Friend #1", surname: "Surname #1", key: '1' },
        { name: "Friend #2", surname: "Surname #1", key: '2' },
        { name: "Friend #3", surname: "Surname #1", key: '3' },
        { name: "Friend #4", surname: "Surname #1", key: '4' },
        { name: "Friend #5", surname: "Surname #1", key: '5' },
        { name: "Friend #6", surname: "Surname #1", key: '6' },
        { name: "Friend #7", surname: "Surname #1", key: '7' },
        { name: "Friend #8", surname: "Surname #1", key: '8' },
        { name: "Friend #9", surname: "Surname #1", key: '9' },
        { name: "Friend #10", surname: "Surname #1", key: '10' }
    ];

    return (
        <View>
            <FlatList
                data={friends}
                renderItem={
                    ({ item }) => {
                        return (
                            <View style={style.element}>
                                <Text style={style.text}>Nazywam się "{item.name}" "{item.surname}"</Text>
                            </View>
                        )
                    }
                }
                keyExtractor={item => item.key}
            />
        </View>
    );
};

const style = StyleSheet.create({
    element: {
        borderColor: "black",
        borderWidth: 2,
        marginBottom: 6,
        padding: 5
    },
    text: {
        fontSize: 20
    }
})

export default ListScreen;