import React, { useState } from "react";
import { View, Text, TextInput, Button, StyleSheet, FlatList } from 'react-native';
import Item from '../components/Item';

const InputScreen = () => {

    const [name, setName] = useState('');
    //const [inputName, setInputName] = useState('');

    const addName = (newName) => {
        setName(newName);
    }
    
    const [friends, setFriends] = useState([]);

    return (
        <View>
            <TextInput
                style={style.inputStyle}
                placeholder="Podaj imię"
                onChangeText={addName}
                //(newName) => {
                //  console.log(newName);
                //  setName(newName);
                //}
                //}
                value={name}
            />

            <Button
                title="Zapisz"
                onPress={
                    () => {
                        // setInputName(name);
                        setFriends([...friends, { name: name }]);
                    }
                }
            />

            <FlatList
                style={style.listStyle}
                data={friends}
                renderItem={
                    ({ item }) => {
                        return (
                            <Item name={item.name} />
                            // <View style={style.itemStyle}>
                            //     <Text style={style.itemTextStyle}>Nazywam się "{item.name}"</Text>
                            // </View>
                        )
                    }
                }
                keyExtractor={item => Math.random().toString()}
            />
        </View>
    )
}

const style = StyleSheet.create(
    {
        inputStyle: {
            borderColor: "black",
            borderWidth: 1,
            padding: 5,
            margin: 5
        },
        listStyle: {
            marginTop: 30
        },
        // itemStyle: {
        //     borderWidth: 2,
        //     borderColor: 'red',
        //     padding: 5,
        //     marginBottom: 4
        // },
        // itemTextStyle: {
        //     textAlign: 'center'
        // }
    }
)

export default InputScreen;