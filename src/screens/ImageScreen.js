import React from 'react';
import { ScrollView } from 'react-native';
import ImageDetails from '../components/ImageDetails';

const ImageScreen = () => {
    return (
        <ScrollView>
            <ImageDetails author='Michał' title='tytuł 1' source={require('../../assets/images/image1.jpg')} />
            <ImageDetails author='Tomek' title='tytuł 2' source={require('../../assets/images/image2.jpg')} />
            <ImageDetails author='Kasia' title='tytuł 3' source={require('../../assets/images/image3.jpg')} />
            <ImageDetails author='Zuzia' title='tytuł 4' source={require('../../assets/images/image4.jpg')} />
            {/* <View style={style.imageStyle}>
                <Image source={require('../../assets/images/image1.jpg')} />
                <Text>Autor: "Autor1"</Text>
                <Text>Tytuł: "Tytuł1"</Text>
            </View>
            <View style={style.imageStyle}>
                <Image source={require('../../assets/images/image2.jpg')} />
                <Text>Autor: "Autor2"</Text>
                <Text>Tytuł: "Tytuł2"</Text>
            </View>
            <View style={style.imageStyle}>
                <Image source={require('../../assets/images/image3.jpg')} />
                <Text>Autor: "Autor3"</Text>
                <Text>Tytuł: "Tytuł3"</Text>
            </View>
            <View style={style.imageStyle}>
                <Image source={require('../../assets/images/image4.jpg')} />
                <Text>Autor: "Autor4"</Text>
                <Text>Tytuł: "Tytuł4"</Text>
            </View> */}
        </ScrollView>
    );
};
export default ImageScreen;