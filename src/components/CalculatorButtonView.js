import React, {useState} from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const CalculatorButtonView = props => {
if (props.digitalOrSign == 'digital') {
    return (
        <TouchableOpacity style={style.button}>
            <Text style={style.textStyleNumber} onPress={() => {props.buttonPress(props.sign)}}>
                {props.sign}
            </Text>
        </TouchableOpacity>
    );
} 
else if (props.digitalOrSign == 'sign') {
    return (
        <TouchableOpacity style={style.button} onPressonPress={() => {props.buttonPress(props.sign)}}>
            <Text style={style.textStyleOperation}>
                {props.sign}
            </Text>
        </TouchableOpacity>
    );
}
    
}

const style = StyleSheet.create(
    {
        button: {
            flex: 1,
            alignItems: 'center',
            alignSelf: 'stretch',
            justifyContent: 'center'
        },
        textStyleNumber: {
            color:"white"
        },
        textStyleOperation: {
            color:"orange"
        },
    }
)
export default CalculatorButtonView;