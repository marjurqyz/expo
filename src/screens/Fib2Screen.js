import React, { useState } from 'react';
import { Text, View, Button, StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

const FibScreen = () => {
    const [f1, setF1] = useState(0);
    const [f2, setF2] = useState(1);
    const [counter, setCounter] = useState(0);

    const calculate = () => {
        setF2(f2 + f1);
        setF1(f2);
        setCounter(counter + 1);
    };

    

    const [fibs, setFibs] = useState([
        // { fib: 0, value: 0 },
        // { fib: 1, value: 1 },
        // { fib: 2, value: 1 },
        // { fib: 3, value: 2 },
        // { fib: 4, value: 3 },
        // { fib: 5, value: 5 },
        // { fib: 5, value: 8 }
    ]);

    return (
        <View>
            <Button title="policz" onPress={() => {

                calculate();
                setFibs([...fibs, { fib: counter, value: f1 }]);
            }} />

            <FlatList
                data={fibs}
                renderItem={
                    ({ item }) => {
                        return (
                            <View style={myStyle.element}>
                                <Text style={myStyle.text}>Fib{item.fib}: {item.value}</Text>
                            </View>
                        )
                    }
                }
                keyExtractor={item => Math.random().toString()}
            />

        </View>
    );
};

const myStyle = StyleSheet.create(
    {
        text: {
            fontSize: 30,
            marginTop: 20
        }
    }
);

export default FibScreen;