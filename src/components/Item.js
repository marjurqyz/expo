import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const Item = ({ name }) => {
    return (
        <TouchableOpacity
            style={style.itemStyle}
            onPress={() => console.log('test123')}
        >
            <Text style={style.itemTextStyle}>Nazywam się "{name}"</Text>
        </TouchableOpacity>
    )
}

const style = StyleSheet.create(
    {
        itemStyle: {
            borderWidth: 2,
            borderColor: 'red',
            padding: 5,
            marginBottom: 4
        },
        itemTextStyle: {
            textAlign: 'center'
        }
    }
)

export default Item;