import React from 'react';
import { View, Text, StyleSheet, Image} from 'react-native';

//const ImageDetails = (props) => {
const ImageDetails = ({author, title, source}) => {

   // console.log(props);

    return (
        <View style={style.imageStyle}>
            <Image source={source} />
            <Text>Autor: {author}</Text>
            <Text>Tytuł: {title}</Text>
        </View>
    );
}

const style = StyleSheet.create(
    {
        imageStyle: {
            borderWidth: 2,
            borderColor: 'red',
            marginBottom: 9,
            padding: 5
        }
    }
)
export default ImageDetails;