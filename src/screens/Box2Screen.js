import React from 'react';
import { View, StyleSheet } from 'react-native';

const Box2Screen = () => {
    return (
        <View style={styles.viewParent}>
            <View style={styles.viewChildFirst} />
            <View style={styles.viewChildSecond} />
            <View style={styles.viewChildThird} />
        </View>
    )
}

const styles = StyleSheet.create({
    viewParent: {
        height: 240,
        borderWidth: 1,
        borderColor: 'red',
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    viewChildFirst: {
        backgroundColor: 'blue',
        width: 80,
        height: 80

    },
    viewChildSecond: {
        backgroundColor: 'yellow',
        width: 80,
        height: 80,
        alignSelf: 'center',
        //  marginTop: 80
        // top: 80,
    },
    viewChildThird: {
        backgroundColor: 'green',
        width: 80,
        height: 80,
        alignSelf: 'flex-end',
    }

});

export default Box2Screen;