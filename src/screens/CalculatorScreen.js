import React, {useState} from "react";
import {View, Text, StyleSheet, TouchableOpacity,} from 'react-native';
import CalculatorButtonView from "../components/CalculatorButtonView";

const CalcScreen = () => {
    const [mathematicalExpression, setMathematicalExpression] = useState('');

    
    const buttonPress = (buttonValue) => {
        // console.log(buttonValue);
        setMathematicalExpression([mathematicalExpression, buttonValue])
    }



    return (
        <View >
        <View style={style.operationBlock}/>
        <View style={style.resultBlock}>
            <Text style={{textAlign: "right", fontSize: 50, color: "white"}}>
                {mathematicalExpression}
            </Text>
        </View>
        <View style={style.container}>
            <View style={style.row}>
            <CalculatorButtonView sign='AC' digitalOrSign='sign' buttonPress={buttonPress} />
            <CalculatorButtonView sign='D' digitalOrSign='sign' buttonPress={buttonPress} />
            <CalculatorButtonView sign='%' digitalOrSign='sign' buttonPress={buttonPress} />
            <CalculatorButtonView sign='/' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
            <View style={style.row}>
            <CalculatorButtonView sign='7' digitalOrSign='digital' buttonPress={buttonPress}  />
            <CalculatorButtonView sign='8' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='9' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='*' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
            <View style={style.row}>
            <CalculatorButtonView sign='4' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='5' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='6' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='-' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
            <View style={style.row}>
            <CalculatorButtonView sign='1' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='2' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='3' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='+' digitalOrSign='sign' buttonPress={buttonPress} />
            </View>
           <View style={style.row}>
           <CalculatorButtonView sign='' digitalOrSign='sign' buttonPress={buttonPress} />
            <CalculatorButtonView sign='0' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='.' digitalOrSign='digital' buttonPress={buttonPress} />
            <CalculatorButtonView sign='=' digitalOrSign='sign' buttonPress={buttonPress} />
        </View>
       
        
        </View>

        </View>

    )
}

const style = StyleSheet.create({


    operationBlock: {
        backgroundColor: "black",
        height:"35%",
        alignSelf:"stretch"
    },
    resultBlock: {
        backgroundColor:"black",
        height:"15%",
        alignSelf:"stretch",
        justifyContent: "flex-end",


    },

    container: {
        backgroundColor:"black",
        height:"50%"


    },
    row: {
        flexDirection: 'row',
        flex: 1,
    },

    


})

export default CalcScreen;