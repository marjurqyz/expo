import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const HomeScreen = ({ navigation }) => {
    return (
        <View>
            <Text style={style.text}>Strona główna</Text>
            <View style={style.button}>
                <Button
                    title="Idż do ButtonScreen"
                    onPress={
                        () => {
                            navigation.navigate('Button');
                        }
                    }
                    color="#eb050c"

                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do VariableScreen"
                    onPress={
                        () => {
                            navigation.navigate('Variable');
                        }
                    }
                    color="#005eff"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do FibScreen"
                    onPress={
                        () => {
                            navigation.navigate('Fib');
                        }
                    }
                    color="#2abd0d"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do Fib2Screen"
                    onPress={
                        () => {
                            navigation.navigate('Fib2');
                        }
                    }
                    color="#2abd0d"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do InputScreen"
                    onPress={
                        () => {
                            navigation.navigate('Input');
                        }
                    }
                    color="#ffd633"
                />
            </View>

            <View style={style.button}>
                <Button
                    title="Idż do ListScreen"
                    onPress={
                        () => {
                            navigation.navigate('List');
                        }
                    }
                    color="#ffc266"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do LayoutScreen"
                    onPress={
                        () => {
                            navigation.navigate('Layout');
                        }
                    }
                    color="black"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do BoxScreen"
                    onPress={
                        () => {
                            navigation.navigate('Box');
                        }
                    }
                    color="black"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do Box2Screen"
                    onPress={
                        () => {
                            navigation.navigate('Box2');
                        }
                    }
                    color="black"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do Box3Screen"
                    onPress={
                        () => {
                            navigation.navigate('Box3');
                        }
                    }
                    color="black"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do ImageScreen"
                    onPress={
                        () => {
                            navigation.navigate('Image');
                        }
                    }
                    color="black"
                />
            </View>
            <View style={style.button}>
                <Button
                    title="Idż do Calculator"
                    onPress={
                        () => {
                            navigation.navigate('Calculator');
                        }
                    }
                    color="black"
                />
            </View>
        </View>
    )
};

const style = StyleSheet.create(
    {
        text: {
            fontSize: 30,
            textAlign: "center",
            marginBottom: 5
        },
        button: {
            marginBottom: 5
        }
    }
);

export default HomeScreen;