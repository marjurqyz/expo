
import React, { useState } from 'react';
import { Text, View, Button, StyleSheet } from 'react-native';

const FibScreen = () => {
    const [f1, setF1] = useState(0);
    const [f2, setF2] = useState(1);
    const [counter, setCounter] = useState(0);

    const calculate = () => {
        setF2(f2 + f1);
        setF1(f2);
        setCounter(counter + 1);
    };

    return (
        <View>
            <Button title="policz" onPress={() => {
                calculate();
            }} />
            <Text style={myStyle.text}>Fib{counter}: {f1}</Text>
        </View>
    );
};

const myStyle = StyleSheet.create(
    {
        text: {
            fontSize: 30,
            marginTop: 20
        }
    }
);

export default FibScreen;