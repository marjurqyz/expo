import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/screens/HomeScreen';
import VariableScreen from './src/screens/VariableScreen';
import ButtonScreen from './src/screens/ButtonScreen';
import FibScreen from './src/screens/FibScreen';
import Fib2Screen from './src/screens/Fib2Screen';
import InputScreen from './src/screens/InputScreen';
import ListScreen from './src/screens/ListScreen';
import LayoutScreen from './src/screens/LayoutScreen';
import BoxScreen from './src/screens/BoxScreen';
import Box2Screen from './src/screens/Box2Screen';
import Box3Screen from './src/screens/Box3Screen';
import ImageScreen from './src/screens/ImageScreen';
import CalculatorScreen from './src/screens/CalculatorScreen';

const MainNavigator = createStackNavigator({
  Home: HomeScreen,
  Variable: VariableScreen,
  Button: ButtonScreen,
  Fib: FibScreen,
  Fib2: Fib2Screen,
  Input: InputScreen,
  List: ListScreen,
  Layout: LayoutScreen,
  Box: BoxScreen,
  Box2: Box2Screen,
  Box3: Box3Screen,
  Image: ImageScreen,
  Calculator: CalculatorScreen
},
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'ETI-II'
    }
  }
);
export default createAppContainer(MainNavigator);