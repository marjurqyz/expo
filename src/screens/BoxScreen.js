import React from "react";
import { View, StyleSheet } from 'react-native';

const BoxScreen = () => {
    return (
        <View style={style.viewStyle}>
            <View style={style.boxChild1Style} />
            <View style={style.viewParen2Style}>
                <View style={style.boxChild2Style} />
            </View>
            <View style={style.boxChild3Style} />
        </View>
    )
}

const style = StyleSheet.create(
    {
        viewStyle: {
            borderColor: 'black',
            borderWidth: 2,
            height: 160,
            flexDirection: 'row',
            justifyContent: 'space-between'
        },

        viewParen2Style: {
            borderColor: 'green',
            borderWidth: 2,
            flexDirection: 'row'
            //alignSelf: 'flex-end'
        },
        boxChild1Style: {
           // borderColor: 'blue',
           // borderWidth: 1,
            width: 80,
            height: 80,
            backgroundColor: 'red'
        },

        boxChild2Style: {
            width: 80,
            height: 80,
            backgroundColor: 'blue',
            //marginTop: 80
            //top: 80
            alignSelf: 'flex-end'
        },
        boxChild3Style: {
            borderColor: 'blue',
            borderWidth: 1,
            width: 80,
            height: 80,
            backgroundColor: 'green'
        }
    }
)
export default BoxScreen;