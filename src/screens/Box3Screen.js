import React from 'react';
import { View, StyleSheet } from 'react-native';

const Box2Screen = () => {
    return (
        <View style={styles.viewParent}>
            <View style={styles.parentRow}>
                <View style={styles.viewChildFirst} />
                <View style={styles.viewChildSecond} />
            </View>
            <View style={styles.parentRowMiddle}>
                <View style={styles.viewChildThird} />
            </View>
            <View style={styles.parentRow}>
                <View style={styles.viewChildFourth} />
                <View style={styles.viewChildFifth} />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    viewParent: {
        height: 340,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: 'red'
    },
    parentRow: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        borderColor: 'black',
        borderWidth: 2
    },
    parentRowMiddle: {
        flexDirection: 'column',
        justifyContent: 'center',
        borderColor: 'black',
        borderWidth: 2
    },

    viewChildFirst: {
        backgroundColor: 'blue',
        width: 80,
        height: 80

    },
    viewChildSecond: {
        backgroundColor: 'yellow',
        width: 80,
        height: 80
    },
    viewChildThird: {
        backgroundColor: 'green',
        width: 80,
        height: 80,

    },
    viewChildFourth: {
        backgroundColor: '#458',
        width: 80,
        height: 80,
    },
    viewChildFifth: {
        backgroundColor: '#322',
        width: 80,
        height: 80
    }
});

export default Box2Screen;