import React, { useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const ButtonScreen = () => {

    //let counter = 0;
    const [counter, setCounter] = useState(0);
    return (
        <View>
            <Text style={myStyle.header}>Button Screen</Text>
            <View style={myStyle.button}>
                <Button
                    title="+"
                    onPress={
                        () => {
                            setCounter(counter + 1);
                        }
                    }
                    color="green"
                />
            </View>
            <View>
                <Button
                    title="-"
                    onPress={
                        () => {
                            setCounter(counter - 1);
                        }
                    }
                    color="red"
                />
            </View>
            <Text style={myStyle.text}>Counter: {counter}</Text>
        </View>
    );
}

const myStyle = StyleSheet.create(
    {
        header: {
            fontSize: 30,
            marginBottom: 10,
            alignContent: "center"
        },
        button: {
            marginBottom: 10
        },
        text: {
            fontSize: 30,
            marginTop: 20
        }
    }
);

export default ButtonScreen;